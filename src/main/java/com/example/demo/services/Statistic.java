package com.example.demo.services;

import com.example.demo.dto.StatisticData;

import java.io.IOException;

public interface Statistic {
    StatisticData getStatistic(String dataFile) throws IOException;
}
